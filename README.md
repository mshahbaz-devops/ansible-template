# Ansible Template

> Note: The instructions below assume a server machine (or VM) running Ubuntu 18.04 with Docker installed.

## Getting Started

- Clone this repository:

```bash
$ cd ~/
$ git clone https://gitlab.com/mshahbaz-devops/ansible-template.git ansible-template
```

- Modify the username and password in [`hosts.ini`](https://gitlab.com/mshahbaz-devops/ansible-template/-/blob/main/hosts.ini#L2) and [Makefile](https://gitlab.com/mshahbaz-devops/ansible-template/-/blob/main/Makefile#L15).

- Also, update the username and VM's IP address in [`ssh.cfg`](https://gitlab.com/mshahbaz-devops/ansible-template/-/blob/main/ssh.cfg#L3).

- Start the `ansible` docker.

```bash
$ cd ~/ansible-template
$ make ansible
```

- Test different playbooks.

  - `$ make helloworld`
    > You can also test the command line version using `$ make cmd-helloworld`
  - `$ make pingall`

That's it. Enjoy!

## APPENDX-Z: Install Prerequisites

```bash
$ sudo apt install make
$ sudo apt install docker.io; sudo usermod -aG docker ${USER}
```
