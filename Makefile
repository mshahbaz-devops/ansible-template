
#### Start Ansible docker ####

ansible:
	sh ./scripts/ansible


#### Run Ansible commands ####

helloworld:
	ansible-playbook playbooks/helloworld.yml

cmd-helloworld:
	ansible all --inventory=10.1.243.71, -m ansible.builtin.shell -a 'echo hello world' \
	--extra-vars "ansible_user=ansible ansible_password=ansible"
	# Note: don't forget to update username and password

pingall:
	ansible-playbook playbooks/pingall.yml

